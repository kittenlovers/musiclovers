INSERT INTO MSL_PRODUCT 
(weight,    price,      name,                   status,     online,     inexpensive,    for_child,  min_age,    professional,   used,   brand_id,   insert_date,            owner_id,   description) values 
(0.09,      25.90,      'Armonica a bocca',     true,       true,       false,          false,      null,       false,          false,  14,        '2015-08-21 17:00:00',   null,       'Il grande suono tradizionale di questa armonica blues deriva da un modello MS con corpo in legno, un''ottima scelta per suonare il blues.'),
(1.52,      3999.00,    'Tromba',               true,       true,       true,           false,      null,       true,           true,   1,         '2015-08-21 17:00:00',   1,          'Adams A4 Bb-trumpet, Custom Series, Heavy version, 3 perinet valves, diameter 12,00mm (.470") L- bore, heavy fixed moutphiece thomann receiver, Adams 3ML leadpipe, one piece diameter 14cm (5.5") goldbrass bell, wall thickness: 0,45mm, brushed gold plated, incl. Adams Soft Bag'),
(0.83,      534.38,     'Clarinetto',           true,       false,      false,          false,      null,       false,          false,  2,         '2015-08-21 17:00:00',   null,       'Amati ACL-201 Bb Student Clarinet (Standard)'),
(7.34,      1298.99,    'Sassofono',            true,       false,      true,           false,      null,       false,          false,  2,         '2015-08-21 17:00:00',   null,       'Amati AS43 Intermediate Alto Saxophone Gold Lacquer'),
(1.35,      950.00,     'Tromba',               false,      false,      false,          false,      null,       true,           false,  2,         '2015-08-21 17:00:00',   null,       'Amati Bb-Trumpet Professional ATR 831'),
(2.70,      199.00,     'Trombone',             true,       true,       true,           false,      null,       true,           true,   32,        '2015-08-21 17:00:00',   2,          'An item that has been used previously'),
(10.89,     799.00,     'Corno Francese',       true,       true,       false,          false,      null,       false,          false,  2,         '2015-08-21 17:00:00',   null,       'Single F, Lacquered Brass Body, Rose Brass Leadpipe, mechanical-link tapered rotary valves, nickel-sleeved outer tuning slides, .472" bore, 11.9" bell, wood-frame case (KC-54B)'),
(6.76,      1690.00,    'Basso Tuba',           true,       true,       true,           false,      null,       false,          false,  32,        '2015-08-21 17:00:00',   null,       'Basso tuba argento'),
(31.00,     7500.00,    'Arpa a pedali',        true,       true,       false,          false,      null,       false,          false,  3,         '2015-08-21 17:00:00',   null,       'Its four-fifths size is ideal for the smaller harpist or for the harpist who requires a small, easily portable pedal harp. In terms of string tension and tone it is similar to the larger members of the Etude range.'),
(3.63,      249.99,     'Banjo',                false,      true,       true,           false,      null,       false,          false,  4,         '2015-08-21 17:00:00',   null,       'With a pickup and available in a black chrome finish'),
(7.30,      358.20,     'Tastiera',             true,       true,       true,           false,      null,       true,           true,   5,         '2015-08-21 17:00:00',   3,          'Keylab 61 è una tastiera MIDI a 61 note con tasti sensibili alla velocity e all''aftertouch, progettata per integrarsi alla perfezione in qualsiasi configurazione in studio o live.'),
(9.00,      1488.00,    'Chitarra Acustica',    true,       true,       false,          false,      null,       true,           false,  6,         '2015-08-21 17:00:00',   null,       'The Award-Winning Babicz SPIDER model.'),
(0.04,      30.00,      'Clarinetto',           true,       true,       false,          true,       5,          false,          false,  7,         '2015-08-21 17:00:00',   null,       'Clarinetto per bambini'),
(8.40,      2000.00,    'Sassofono',            true,       true,       false,          false,      null,       true,           false,  8,         '2015-08-21 17:00:00',   null,       'We designed the Big Bell Stone Series Saxophones for you to be able to play without limits, feeling each note vibrate in your hands. The combination of fine hand-engraving and semi-precious natural stone touch pieces from the deep earth make each saxophone an art piece in itself.'),
(1.23,      659.00,     'Tromba',               false,      true,       true,           false,      null,       true,           true,   8,         '2015-08-21 17:00:00',   4,          'With its sleek design and light bell, the Lynx is the perfect trumpet for "blow your socks off" high notes and zingy, vibrant tone. The new, sleek braces give the trumpet a unique, modern look and are specifically designed to promote free vibration throughout the range of the horn, while improving tone and intonation. '),
(0.60,      89.95,      'Flauto',               true,       true,       false,          false,      null,       true,           true,   8,         '2015-08-21 17:00:00',   5,          'Sterling-silver Royal Crown headjoint, body, & foot; Silver-plated Nickel-silver keys; Open hole; B Foot with Gizmo; Pointed Arms.'),
(3.85,      1109.00,    'Basso',                true,       true,       false,          false,      null,       false,          false,  9,         '2015-08-21 17:00:00',   null,       'Standard 4-String Bass'),
(4.35,      924.00,     'Basso',                false,      false,      false,          false,      null,       true,           true,   9,         '2015-08-21 17:00:00',   6,          '4-String Passive Bolt-On Bass'),
(4.72,      1709.00,    'Basso',                false,      true,       false,          false,      null,       false,          false,  9,         '2015-08-21 17:00:00',   null,       'Icon Standard 6-String Bass'),
(4.54,      1129.00,    'Basso',                true,       true,       false,          false,      null,       false,          false,  9,         '2015-08-21 17:00:00',   null,       'Extended Scale 6-String Bass'),
(14.5,      660.00,     'Tamburo',              true,       true,       false,          false,      null,       true,           true,   10,        '2015-08-21 17:00:00',   8,          'Dynasty MS-XZ14BK DFZ 14" Marching Snare, Black'),
(2.67,      669.00,     'Trombone',             true,       true,       false,          false,      null,       false,          false,  11,        '2015-08-21 17:00:00',   null,       'A brand-new, unused, unopened, undamaged item in its original packaging'),
(0.40,      399.00,     'Flauto',               true,       true,       false,          false,      null,       false,          false,  12,        '2015-08-21 17:00:00',   null,       'The Gemeinhardt 2SP student flute has been a standard and reliable instrument choice for decades and remains the most popular woodwind instrument for beginning flutists'),
(1.26,      1089.00,    'Tromba',               false,      false,      true,           false,      null,       false,          false,  13,        '2015-08-21 17:00:00',   null,       'Getzen 390 Series Student Bb Trumpet'),
(0.69,      349.99,     'Ukulele',              true,       true,       false,          false,      null,       false,          false,  16,        '2015-08-21 17:00:00',   null,       'Plays great, no damage'),
(340.00,    9799.00,    'Piano',                true,       true,       false,          true,       5,          false,          false,  17,        '2015-08-21 17:00:00',   null,       'Kawai baby grand piano'),
(6.50,      806.40,     'Tastiera',             true,       true,       true,           false,      null,       false,          false,  18,        '2015-08-21 17:00:00',   null,       'On Stage. In viaggio. On Top. offre full-length, campioni unlooped di ogni chiave per un suono di pianoforte spettacolare, questa nuova tastiera ridefinisce le vostre aspettative per uno strumento in questa classe. Prendendo il nome dalla parola greca che significa "colore", Krome è il nuovo standard per l''eccellenza sonora in tastiera un musicista gigging, fornendo una gamma illimitata di suono per portare ispirazione vivace la vostra musica.'),
(39.00,     37000.00,   'Arpa',                 true,       true,       false,          false,      null,       true,           false,  19,        '2015-08-21 17:00:00',   null,       'Introduced by Lyon & Healy in 1928, the Salzedo is named after legendary harpist-composer-teacher Carlos Salzedo, who collaborated with Witold Gordon on its design.'),
(2.86,      6900.00,    'Chitarra',             true,       true,       false,          false,      null,       false,          false,  20,        '2015-08-21 17:00:00',   null,       'Stephen Marchione VT with a ONE PIECE light weight mahogany body and neck'),
(0.75,      7950.00,    'Oboe',                 true,       false,      false,          false,      null,       false,          false,  21,        '2015-08-21 17:00:00',   null,       'The oboe possesses good flexibilty of tone and has an innovative keywork design for the left hand feather keys and bottom right hand group of keys.'),
(370.00,    9470.00,    'Piano',                true,       true,       false,          false,      null,       true,           true,   22,        '2015-08-21 17:00:00',   9,          'Privately owned and thoughtfully maintained over the years.'),
(11.35,     8895.00,    'Corno',                false,      false,      false,          false,      null,       false,          false,  23,        '2015-08-21 17:00:00',   null,       'The bell is medium large bore, the optimum for this type of instrument, made in yellow brass for a characteristic warm, yet exciting sound.'),
(0.25,      475.00,     'Flauto',               true,       true,       false,          false,      null,       true,           true,   24,        '2015-08-21 17:00:00',   10,         'Standard model Verne Q Powell sterling silver flute head joint'),
(5.26,      2500.00,    'Chitarra',             true,       true,       false,          false,      null,       false,          false,  25,        '2015-08-21 17:00:00',   null,       'This fully-acoustic aluminum Specimen guitar has quite a usable volume unamplified.'),
(4.75,      200.00,     'Mandolino',            true,       true,       true,           false,      null,       false,          false,  26,        '2015-08-21 17:00:00',   null,       'Un Mandolino economico per principianti'),
(380.85,    14900.00,   'Piano',                true,       true,       false,          false,      null,       true,           true,   27,        '2015-08-21 17:00:00',   12,         'This Steinway M was restored approximately 15 years ago with an extensive amount of work performed. A truly great sounding and playing piano.'),
(1.34,      105.00,     'Tromba',               true,       false,      false,          false,      null,       true,           true,   28,        '2015-08-21 17:00:00',   13,         'This trumpet is in used average condition.'),	
(15.87,     2555.00,    'Piano Elettrico',      true,       false,      false,          false,      null,       false,          false,  29,        '2015-08-21 17:00:00',   null,       'While these pianos have such elements in common with vintage Rhodes pianos, nearly every system has been redesigned to maximize playability and minimize weight.'),
(2.50,      500.00,     'Chitarra',             true,       true,       false,          false,      null,       true,           true,   30,        '2015-08-21 17:00:00',   1,          'Ottimo stato'),
(7.90,      5999.00,    'Sassofono',            true,       true,       false,          false,      null,       true,           false,  32,        '2015-08-21 17:00:00',   null,       'With a heritage of instrument-making in Japan that traces back to 1893, Yanagisawa is recognized today as a maker of the most carefully handcrafted family of artist-level saxophones in the world. In models ranging from sopranino to baritone, these superb instruments have won the praise of many celebrated musicians'),
(11.00,     950.00,     'Arpa celtica',         true,       true,       false,          false,      null,       false,          false,  3,         '2015-08-21 17:00:00',   null,       'Arpa celtica (a levette) Aoyama mod. Kerry vendo. 34 corde, peso 11 kg, altezza 1,44 cm, larghezza 65 cm. Tensione delle corde media.'),
(7.30,      399.00,     'Tastiera',             true,       false,      false,          false,      null,       false,          false,  5,         '2015-08-21 17:00:00',   null,       'Arturia Keylab 61 Black Edition è una tastiera MIDI a 61 note con tasti sensibili alla velocity e all''aftertouch, progettata per integrarsi alla perfezione in qualsiasi configurazione in studio o live. Anche in questo caso il controller a 61 tasti viene fornito con i software Analog Lab, Solina V, Prophet V e UVI Grand Piano Model D e offre un’esperienza senza precedenti grazie ai sintetizzatori TAE. Keylab 61 Black Edition è un sintetizzatore ibrido professionale, e offre una combinazione perfetta tra l''immediatezza di un sintetizzatore hardware e la flessibilità di una soluzione software. Keylab 61 è completamente preimpostato per funzionare con il software Analog Lab, e consente di avere subito il controllo approfondito del suono. Potete modificare il cutoff e la risonanza del filtro, modificare i due inviluppi ADSR e molto altro. Ma Keylab 61 funziona anche come controller MIDI universale compatibile con qualsiasi software o hardware di terze parti. Potete configurare le assegnazioni MIDI per qualsiasi hardware o software utilizzando lo schermo LED o il software MIDI Control Center fornito in dotazione.'),
(1.35,      599.00,     'Tromba',               true,       true,       false,          false,      null,        false,         false,  28,        '2015-08-21 17:00:00',   null,       'Bach TR501 combina un canneggio medio-grande 11,68 mm (0,460" ) con mouthpipe ramato per una maggiore durata e facilità nella produzione del suono. I pistoni in Monel inoltre offrono un azione regolare.'),
(8.15,      500.00,     'Sassofono',            true,       true,       false,          false,      null,       true,           true,   11,        '2015-08-21 17:00:00',   2,          'Si trova in perfette condizioni originali, è stato utilizzato una decina di volte e poi messo via. La meccanica è perfetta, la laccatura è ottima ed i tamponi in pelle sono come nuovi.'),
(7.55,      3499.00,    'Sassofono',            true,       true,       false,          false,      null,       true,           false,  8,         '2015-08-21 17:00:00',   null,       'Sax Tenore a "Campana Larga" (BIG BELL) per enfatizzare gli armonici più gravi e per creare un suono più ricco e pieno!'),
(3.60,      650.00,     'Chitarra elettrica',   true,       false,      false,          false,      null,       true,           true,   9,         '2015-08-21 17:00:00',   3,          'Carvin DC 125 F in buonissime condizioni visti i suoi anni, necktrough body e ponte originale kahler'),
(1.40,      500.00,     'Flicorno',             false,      true,       true,           false,      null,       true,           true,   13,        '2015-08-21 17:00:00',   4,          'Un flicorno Getzen usato pochissimo, come nuovo'),
(0.08,      26.90,      'Armonica a bocca',     false,      true,       false,          false,      null,       false,          false,  14,        '2015-08-21 17:00:00',   null,       'Il grande suono tradizionale di questa armonica blues deriva da un modello MS con corpo in legno, un''ottima scelta per suonare il blues. Questa armonica è costruita secondo lo standard Richter "Modular System". Le armoniche costruite secondo il sistema modulare Richter hanno tutte le parti di tutti i modelli MS combinabili tra loro. Grazie ad un semplice sistema di viti, il musicista potrà facilmente pulire l''armonica e soprattutto combinare le parti per adeguare lo strumento alle proprie esigenze.'),
(28.50,     799.00,     'Pianoforte digitale',  true,       true,       false,          false,      null,       false,          false,  17,        '2015-08-21 17:00:00',   null,       'La gamma Compact Line che costituisce uneccellente introduzione ai pianoforte digitali Kawai, è dotata di una meccanica con tastiera pesata, una selezione di suoni di alta qualità e ha un mobile dal design moderno.Inoltre, il CL26 utilizza lesclusiva tecnologia Harmonic ImagingTM, con campionamento del suono di pianoforte su ogni singolo tasto, che ne arricchisce lautenticità timbrica.'),
(0.56,      2400.00,    'Flauto traverso',      true,       true,       false,          false,      null,       true,           false,  24,        '2015-08-21 17:00:00',   null,       'Verne Q. Powell PS-750-I Sonaré Piccolo, Indien Onyx wood, stainless steel mechanism, art deco style mechanism, pitch a''= 442 Hz, modern Powell scale, Prestini pads, stainless steel springs, classic hand cut headjoint, incl. zippered black case, swab stick and silk swab'),
(7.10,      422.00,     'Pianoforte digitale',  true,       true,       false,          false,      null,       true,           false,  31,        '2015-08-21 17:00:00',   null,       'Tastiera Graded Soft Touch con il tocco leggero delle tastiere digitali, 76 tasti, polifonia 32, 500 suoni. 165 stili di accompagnamento, 9 tipi di reverbero, 4 tipi di Chorus, 26 tipi di Harmony, Preset-Master EQ (5 Tipi)'),
(22.00,     749.00,     'Pianoforte digitale',  false,      true,       false,          false,      null,       true,           false,  31,        '2015-08-21 17:00:00',   null,       'Pianoforte Yamaha DGX-650 White per uso professionistico'),
(38.00,     898.00,     'Pianoforte digitale',  true,       false,      false,          false,      null,       true,           false,  31,        '2015-08-21 17:00:00',   null,       'Il piano digitale Arius YDP-142 offre un tocco piano autentico e un suono adatto per ogni aspirante pianista. La meccanica Graded Hammer Standard (GHS) consente di apprendere la tecnica di diteggiatura adatta per una facile transizione ai pianoforti acustici. Pure CF Sound Engine offre le registrazioni espressive di un pianoforte a coda da concerto Yamaha.'),
(3.80,      299.00,     'Chitarra elettrica',   true,       true,       true,           false,      null,       false,          true,   30,        '2015-08-21 17:00:00',   5,          'Chitarra elettrica solid body, costruzione "bolt on", scala 25,5", corpo in tiglio, manico in acero con trussrod a doppia azione, tastiera in palissandro a 24 tasti'),
(1.10,      173.00,     'Chitarra acustica',    true,       true,       false,          false,      null,       false,          true,   30,        '2015-08-21 17:00:00',   6,          'Ottima chitarra in buono stato in vendita ad un ottimo prezzo'),
(3.90,      437.68,     'Chitarra acustica',    false,      true,       false,          false,      null,       true,           false,  30,        '2015-08-21 17:00:00',   null,       'Washburn Guitarra electrica cuerpo hueco. Guitarra eléctrica de caja, Tapa, aros y fondo de arce flameado, Mástil de arce de 22 trastes, Diapasón de palorrosa con Buzz Feiten tuning system e incrustaciones de nácar, 2 pastillas dobles WB630, Puente fijo Tune-o-matic, Selector de pastillas de 3 posiciones, 2 controles de volumen, 2 controles de tono, Clavijero Grover, Herrajes dorados,'),
(6.00,      548.97,     'Chitarra elettrica',   true,       true,       false,          false,      null,       true,           false,  30,        '2015-08-21 17:00:00',   null,       'WGWR. PXM-corpo solido 200AFTBLMGUITARRA ELETTRICA 6 CORDE arcuate fiammato top in acero, corpo in tiglio, manico in acero set-collo, 24-fret ebano, 2 humbucker Seymour Duncan attivi, 3 posizioni, 1 piatto. vol, 1 tono, ponte fisso, Buzz Feiten Tuning System Grover 3 + 3 paletta.'),
(5.00,      553.57,     'Chitarra elettrica',   false,      true,       true,           false,      null,       true,           false,  30,        '2015-08-21 17:00:00',   null,       'Corpo in acero. Manico in acero con trussrod a doppia azione. Tastiera in palissandro a 22 tasti con segnatasti madreperlati, ponte tune-o-matic, selettore dei pickup a tre vie, due controlli del volume e due controlli dei toni. Due pickup Humbucker (manico e ponte). meccaniche dorate Grover Exclusive. hardware dorato. Disponibile nei colori natural (N), wine red (WR), nero (BK), bianca (WH), finitura lucida.'),
(4.50,      629.57,     'Chitarra elettrica',   true,       false,      false,          false,      null,       false,          true,   30,        '2015-08-21 17:00:00',   1,          'Chitarra elettrica Nuno Bettencourt Signature model, scala 25,5", costruzione "bolt on", corpo in ontano, manico in acero con trussrod a doppia azione, tastiera in palissandro a 22 tasti. Colore rosso senza finitura.'),
(5.00,      265.85,     'Chitarra elettrica',   true,       true,       false,          false,      null,       false,          false,  30,        '2015-08-21 17:00:00',   null,       'Chitarra elettrica solid body, corpo in tiglio, manico bolt-on in acero, tastiera in palissandro.');




INSERT INTO MSL_PRODUCT_IMAGES(image, product_id) values
('img/products/p01.jpg',    1),
('img/products/p02a.jpg',   2),
('img/products/p02b.jpg',   2),
('img/products/p02c.jpg',   2),
('img/products/p02d.jpg',   2),
('img/products/p02e.jpg',   2),
('img/products/p02g.jpg',   2),
('img/products/p03.jpg',    3),
('img/products/p04.jpg',    4),
('img/products/p05.jpg',    5),
('img/products/p06a.jpg',   6),
('img/products/p06b.jpg',   6),
('img/products/p07.jpg',    7),
('img/products/p08.jpg',    8),
('img/products/p09.jpg',    9),
('img/products/p10.jpg',    10),
('img/products/p11.jpg',    11),
('img/products/p12a.jpg',   12),
('img/products/p12b.jpg',   12),
('img/products/p12c.jpg',   12),
('img/products/p12d.jpg',   12),
('img/products/p12e.jpg',   12),
('img/products/p12f.jpg',   12),
('img/products/p12g.jpg',   12),
('img/products/p13.jpg',    13),
('img/products/p14.jpg',    14),
('img/products/p15.jpg',    15),
('img/products/p16.jpg',    16),
('img/products/p17.jpg',    17),
('img/products/p18.jpg',    18),
('img/products/p19.jpg',    19),
('img/products/p20.jpg',    20),
('img/products/p21.jpg',    21),
('img/products/p22.jpg',    22),
('img/products/p23.jpg',    23),
('img/products/p24.jpg',    24),
('img/products/p25.jpg',    25),
('img/products/p26a.jpg',   26),
('img/products/p26b.jpg',   26),
('img/products/p26c.jpg',   26),
('img/products/p26d.jpg',   26),
('img/products/p26e.jpg',   26),
('img/products/p26f.jpg',   26),
('img/products/p26g.jpg',   26),
('img/products/p26h.jpg',   26),
('img/products/p26i.jpg',   26),
('img/products/p27.jpg',    27),
('img/products/p28.jpg',    28),
('img/products/p29a.jpg',   29),
('img/products/p29b.jpg',   29),
('img/products/p29c.jpg',   29),
('img/products/p29d.jpg',   29),
('img/products/p29f.jpg',   29),
('img/products/p29e.jpg',   29),
('img/products/p30.jpg',    30),
('img/products/p31.jpg',    31),
('img/products/p32.jpg',    32),
('img/products/p33a.jpg',   33),
('img/products/p33b.jpg',   33),
('img/products/p33c.jpg',   33),
('img/products/p33d.jpg',   33),
('img/products/p33e.jpg',   33),
('img/products/p34a.jpg',   34),
('img/products/p34b.jpg',   34),
('img/products/p34c.jpg',   34),
('img/products/p34d.jpg',   34),
('img/products/p35.jpg',    35),
('img/products/p36a.jpg',   36),
('img/products/p36b.jpg',   36),
('img/products/p37.jpg',    37),
('img/products/p38.jpg',    38),
('img/products/p39.jpg',    39),
('img/products/p40.png',    40),
('img/products/p41.jpg',    41),
('img/products/p42.jpg',    42),
('img/products/p43.jpg',    43),
('img/products/p44a.jpg',   44),
('img/products/p44b.jpg',   44),
('img/products/p45a.jpg',   45),
('img/products/p45b.jpg',   45),
('img/products/p45c.jpg',   45),
('img/products/p45d.jpg',   45),
('img/products/p46a.jpg',   46),
('img/products/p46b.jpg',   46),
('img/products/p46c.jpg',   46),
('img/products/p46d.jpg',   46),
('img/products/p46e.jpg',   46),
('img/products/p47a.jpg',   47),
('img/products/p47b.jpg',   47),
('img/products/p47c.jpg',   47),
('img/products/p47d.jpg',   47),
('img/products/p48.jpg',    48),
('img/products/p49.jpg',    49),
('img/products/p50a.jpg',   50),
('img/products/p50b.jpg',   50),
('img/products/p50c.jpg',   50),
('img/products/p50d.jpg',   50),
('img/products/p51a.jpg',   51),
('img/products/p51b.jpg',   51),
('img/products/p51c.jpg',   51),
('img/products/p52.jpg',    52),
('img/products/p53a.jpg',   53),
('img/products/p53b.jpg',   53),
('img/products/p53c.jpg',   53),
('img/products/p53d.jpg',   53),
('img/products/p53e.jpg',   53),
('img/products/p54.jpg',    54),
('img/products/p55a.jpg',   55),
('img/products/p55b.jpg',   55),
('img/products/p56.jpg',    56),
('img/products/p57a.jpg',   57),
('img/products/p57b.jpg',   57),
('img/products/p57c.jpg',   57),
('img/products/p57d.jpg',   57),
('img/products/p57e.jpg',   57),
('img/products/p57f.jpg',   57),
('img/products/p57g.jpg',   57),
('img/products/p57h.jpg',   57),
('img/products/p57h.jpg',   57),
('img/products/p58.jpg',    58),
('img/products/p59.jpg',    59),
('img/products/p60.jpg',    60);