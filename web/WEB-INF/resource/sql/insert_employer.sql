INSERT INTO MSL_EMPLOYER(code, birthdate, name, surname, account_id) values
('marian01', '1991-11-03', 'Marian', 'Solomon', 1),
('donato02', '1975-04-23', 'Donato', 'Cavallo', 2),
('massimo03', '1963-10-04', 'Massimo', 'Della Pena', 3),
('giorgio04', '1985-11-09', 'Giorgio', 'Eliotropo', 4),
('isabella05', '1978-12-05', 'Isabella', 'Fiori', 5),
('valentina06', '1985-03-12', 'Valentina', 'Granata', 6),
('elio07', '1972-06-08', 'Elio', 'Indaco', 7),
('tiziano08', '1968-03-17', 'Tiziano', 'Lavanda', 8),
('cristina09', '1980-04-27', 'Cristina', 'Magenta', 9),
('nicola10', '1979-12-05', 'Nicola', 'Mogano', 10);