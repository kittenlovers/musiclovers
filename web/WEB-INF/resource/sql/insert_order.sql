INSERT INTO MSL_ORDER
(price,     sold_date,              payment_type,       product_id, employer_id,    buyer_id) values
(3999.00,   '2015-08-21 17:00:00',  'Assegno',          2,          1,              19),
(199.00,    '2015-08-21 17:30:00',  'Contanti',         6,          2,              20),
(358.20,    '2015-08-21 18:00:00',  'Carta di credito', 11,         3,              1),
(659.00,    '2015-08-21 19:00:00',  'Carta di credito', 15,         4,              1),
(999.00,    '2015-08-21 19:00:00',  'Carta di credito', 1,          4,              1),
(888.00,    '2015-08-21 19:00:00',  'Carta di credito', 3,         4,              1),
(89.95,     '2015-08-22 12:30:00',  'Contanti',         16,         5,              1);