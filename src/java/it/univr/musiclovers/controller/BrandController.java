package it.univr.musiclovers.controller;

import it.univr.musiclovers.model.BrandModel;
import it.univr.musiclovers.model.ProductModel;
import it.univr.musiclovers.model.beans.BrandBean;
import it.univr.musiclovers.model.beans.ProductBean;
import java.io.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

/**
 *
 * @author Marian Solomon
 */
@ManagedBean
@SessionScoped
public class BrandController extends ControllerModel {

    private transient Part file;
    private BrandBean selectedBrand;
    private static final long serialVersionUID = 1L;

    public String getBrand(int brandID) {
        return addParam(normalizeUrl("brand"), "brandID", String.valueOf(brandID));
    }

    public String getBrandInfo(int brandID) {
        return addParam(normalizeUrl("/brand"), "brandID", String.valueOf(brandID));
    }

    public List<BrandBean> getBrands() throws SQLException {
        return BrandModel.getBrands();
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public List<ProductBean> getProducts() throws SQLException {
        return ProductModel.getProductsByBrand(selectedBrand.getID());
    }

    public BrandBean getSelectedBrand() {
        return selectedBrand;
    }

    public void setSelectedBrand(int brandId) throws SQLException {
        this.selectedBrand = BrandModel.getBrand(brandId);
    }

    public String processBrandForm() throws SQLException {
        if (selectedBrand.getID() > 0) {
            BrandModel.editBrand(selectedBrand);
        } else {
            BrandModel.insertBrand(selectedBrand);
        }
        return "index.xhtml";
    }

    public void processImage() {
        if (file != null) {
            try (InputStream inputStream = file.getInputStream()) {
                String pathWebBuild = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext()).getRealPath("");
                String pathWebSrc = pathWebBuild + "/img/brands/";
                File outputFile = new File(pathWebSrc + File.separator + File.separator + getFilename(file));
                try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
                    byte[] buffer = new byte[4096];
                    int bytesRead = 0;
                    while (true) {
                        bytesRead = inputStream.read(buffer);
                        if (bytesRead > 0) {
                            outputStream.write(buffer, 0, bytesRead);
                        } else {
                            break;
                        }
                    }
                }
                selectedBrand.setLogo("img/brands/" + getFilename(file));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void removeBrand(int brandID) throws SQLException, ParseException {
        if (BrandModel.getBrandsByProduct(brandID).isEmpty()) {
            BrandModel.removeBrand(brandID);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore", ":  impossibile eliminare un brand a cui vi sono ancora prodotti associati!"));
        }
    }

    public void removeLogo(String logo) throws SQLException {
        selectedBrand.setLogo("img/image-not-found.png");
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

}
