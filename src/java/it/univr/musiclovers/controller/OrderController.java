package it.univr.musiclovers.controller;

import static it.univr.musiclovers.controller.ControllerModel.redirectString;
import it.univr.musiclovers.model.CustomerModel;
import it.univr.musiclovers.model.OrderModel;
import it.univr.musiclovers.model.ProductModel;
import it.univr.musiclovers.model.beans.EmployerBean;
import it.univr.musiclovers.model.beans.OrderBean;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Marian Solomon
 */
@ManagedBean
@SessionScoped
public class OrderController extends ControllerModel {

    private float original;
    private int reduction;
    private OrderBean selectedOrder;
    private static final long serialVersionUID = 1L;

    public List<EmployerBean> getEmployers() throws SQLException {
        return OrderModel.getEmployers();
    }

    public String getOrder(int orderID) {
        return addParam(normalizeUrl("order"), "orderID", String.valueOf(orderID));
    }

    public List<OrderBean> getOrders() throws SQLException, ParseException {
        return OrderModel.getOrders();
    }

    public List<OrderBean> getOrdersByBuyer(int buyerID) throws SQLException, ParseException {
        return OrderModel.getOrdersByBuyer(buyerID);
    }

    public List<OrderBean> getOrdersBySeller(int sellerID) throws SQLException, ParseException {
        return OrderModel.getOrdersBySeller(sellerID);
    }

    public void setPrice(int productID) throws SQLException {
        selectedOrder.setPrice(ProductModel.getProduct(productID).getPrice());
        original = selectedOrder.getPrice();
        updatePrice();
    }

    public int getReduction() {
        return reduction;
    }

    public void setReduction(int customerID) throws SQLException {
        if (CustomerModel.isProfessional(customerID)) {
            reduction = CustomerModel.getProfessional(customerID).getReduction();
        } else {
            reduction = 0;
        }
        updatePrice();
    }

    public OrderBean getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(int orderID) throws SQLException, ParseException {
        this.selectedOrder = OrderModel.getOrder(orderID);
    }

    public String processOrderForm() throws SQLException {
        if (selectedOrder != null) {
            if (selectedOrder.getID() > 0) {
                OrderModel.editOrder(selectedOrder);
            } else {
                OrderModel.insertOrder(selectedOrder);
            }
        }
        return redirectString("index.xhtml");
    }

    public void removeOrder(int orderID) throws SQLException {
        OrderModel.removeOrder(orderID);
    }

    private void updatePrice() {
        selectedOrder.setPrice(original - original / 100 * reduction);
    }

}
