package it.univr.musiclovers.controller;

import it.univr.musiclovers.model.CustomerModel;
import it.univr.musiclovers.model.EmployerModel;
import it.univr.musiclovers.model.OrderModel;
import it.univr.musiclovers.model.ProductModel;
import it.univr.musiclovers.model.beans.AccountBean;
import it.univr.musiclovers.model.beans.EmployerBean;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Luca Zidda
 */
@ManagedBean
@SessionScoped
public class EmployerController extends ControllerModel {

    private AccountBean selectedAccount;
    private static final long serialVersionUID = 1L;

    public String getEmployer(int employerID) {
        return addParam(normalizeUrl("employer"), "employerID", String.valueOf(employerID));
    }

    public EmployerBean getEmployer() {
        return (EmployerBean) selectedAccount.getPerson();
    }

    public List<EmployerBean> getEmployers() throws SQLException {
        return EmployerModel.getEmployers();
    }

    public AccountBean getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedEmployer(int employerID) throws SQLException {
        this.selectedAccount = EmployerModel.getAccountByEmployer(employerID);
    }

    public String processCustomerForm() throws SQLException {
        if (selectedAccount.getID() > 0) {
            EmployerModel.editEmployer(selectedAccount);
        } else {
            EmployerModel.insertEmployer(selectedAccount);
        }
        return redirectString("index.xhtml");
    }

    public void removeEmployer(int employerID) throws SQLException, ParseException {
        if (OrderModel.getOrdersBySeller(employerID).isEmpty()) {
            EmployerModel.removeEmployer(employerID);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore", ":  impossibile eliminare un dipendente a cui vi sono ancora ordini associati!"));
        }
    }

}
