package it.univr.musiclovers.controller;

import it.univr.musiclovers.model.CustomerModel;
import it.univr.musiclovers.model.OrderModel;
import it.univr.musiclovers.model.ProductModel;
import it.univr.musiclovers.model.beans.AccountBean;
import it.univr.musiclovers.model.beans.CustomerBean;
import it.univr.musiclovers.model.beans.ProfessionalBean;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Luca Zidda
 */
@ManagedBean
@SessionScoped
public class CustomerController extends ControllerModel {

    private AccountBean accountBean;
    private boolean isProfessional;
    private ProfessionalBean professionalBean;
    private CustomerBean selectedCustomer;
    private static final long serialVersionUID = 1L;

    public AccountBean getAccountBean() {
        return accountBean;
    }

    public String getCustomer(int customerID) {
        return addParam(normalizeUrl("customer"), "customerID", String.valueOf(customerID));
    }

    public List<CustomerBean> getCustomers() throws SQLException {
        return CustomerModel.getCustomers();
    }

    public ProfessionalBean getProfessionalBean() {
        return professionalBean;
    }

    public List<Integer> getProfessionalIDs() throws SQLException {
        return CustomerModel.getProfessionalIDs();
    }

    public List<ProfessionalBean> getProfessionals() throws SQLException {
        return CustomerModel.getProfessionals();
    }

    public CustomerBean getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(int customerID) throws SQLException {
        this.selectedCustomer = CustomerModel.getCustomer(customerID);
        this.accountBean = CustomerModel.getAccount(CustomerModel.getProfessional(customerID).getAccountID());
        this.professionalBean = (ProfessionalBean) accountBean.getPerson();
    }

    public boolean isIsProfessional() {
        return isProfessional;
    }

    public void setIsProfessional(boolean isProfessional) {
        this.isProfessional = isProfessional;
    }

    public String processCustomerForm() throws SQLException {
        if (selectedCustomer != null && professionalBean != null) {
            if (selectedCustomer.getID() > 0) {
                CustomerModel.editCustomer(selectedCustomer);
                if (isIsProfessional() && accountBean != null) {
                    if (((CustomerBean) accountBean.getPerson()).getID() > 0) {
                        CustomerModel.editProfessional(accountBean);
                    } else {
                        CustomerModel.insertProfessional(accountBean);
                    }
                }
            } else {
                CustomerModel.insertCustomer(selectedCustomer);
                if (isIsProfessional()) {
                    ProfessionalBean pb = new ProfessionalBean(selectedCustomer, this.professionalBean.getRole(), this.professionalBean.getReduction());
                    accountBean.setPerson(pb);
                    CustomerModel.insertProfessional(accountBean);
                }
            }
        }
        return redirectString("index.xhtml");
    }

    public void removeCustomer(int customerID) throws SQLException, ParseException {
        if (OrderModel.getOrdersByCustomer(customerID).isEmpty() && ProductModel.getProductsByOwner(customerID).isEmpty()) {
            CustomerModel.removeCustomer(customerID);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Errore", ":  impossibile eliminare un cliente a cui vi sono ancora ordini associati!"));
        }
    }

}
