package it.univr.musiclovers.controller;

import it.univr.musiclovers.model.CustomerModel;
import it.univr.musiclovers.model.EmployerModel;
import it.univr.musiclovers.model.beans.AccountBean;
import it.univr.musiclovers.model.beans.CustomerBean;
import it.univr.musiclovers.model.beans.PersonBean;
import java.io.IOException;
import java.sql.SQLException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author blasco991
 */
@ManagedBean
@SessionScoped
public class AuthenticationController extends ControllerModel {

    private AccountBean accountBean = new AccountBean();
    private boolean logged = false;
    private static final long serialVersionUID = 1L;

    public void check() throws IOException {
        if (isLogged() && accountBean.isEmployer()) {
            redirect("employer/");
        } else if (isLogged() && accountBean.isProfessional()) {
            redirect("customer/");
        }
    }

    public void checkEmployer() throws IOException {
        if (!isLogged() || !accountBean.isEmployer()) {
            redirect("login");
        }
    }

    public void checkProfessional() throws IOException {
        if (!isLogged() || !accountBean.isProfessional()) {
            redirect("login");
        }
    }

    public String getPassword() {
        return accountBean.getPassword();
    }

    public void setPassword(String password) {
        accountBean.setPassword(password);
    }

    public int getProfessionalID() {
        return ((CustomerBean) accountBean.getPerson()).getID();
    }

    public String getUsername() {
        return accountBean.getUsername();
    }

    public void setUsername(String username) {
        accountBean.setUsername(username);
    }

    public boolean isLogged() {
        return logged;
    }

    public String login(String componendID, AccountBean account, String outcome) {
        if (account != null && account.getPerson() != null) {
            accountBean = account;
            logged = true;
            return redirectString(outcome);
        } else {
            FacesContext.getCurrentInstance().addMessage(componendID, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username o Password errati!", "Username o Password errati!"));
            return addExt("login");
        }
    }

    public String loginEmployer() throws SQLException {
        AccountBean account = EmployerModel.getAccount(accountBean.getUsername(), accountBean.getPassword());
        return login("loginEmployer", account, "employer/");
    }

    public String loginProfessional() throws SQLException {
        AccountBean account = CustomerModel.getAccount(accountBean.getUsername(), accountBean.getPassword());
        return login("loginProfessional", account, "customer/");
    }

    public String logout() {
        accountBean = new AccountBean();
        logged = false;
        return redirectString("/homepage");
    }

}
