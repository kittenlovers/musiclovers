package it.univr.musiclovers.model;

import it.univr.musiclovers.model.beans.ProductBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;

/**
 *
 * @author Marian Solomon
 */
public abstract class ProductModel extends Model {

    private static final String queryEditProduct = "UPDATE " + getTablePrefix() + "_product SET status = ?, online = ?, weight = ?, price = ?, name = ?, description = ?, inexpensive = ?, professional = ?, for_child = ?, used = ?, min_age = ?, brand_id = ? WHERE id = ?";
    private static final String queryGetOnlineProducts = "SELECT * FROM " + getTablePrefix() + "_PRODUCT WHERE online = 'true'";
    private static final String queryGetProduct = "SELECT * FROM " + getTablePrefix() + "_PRODUCT WHERE id = ?";
    private static final String queryGetProductImages = "SELECT image FROM " + getTablePrefix() + "_product_images WHERE product_id = ?";
    private static final String queryGetProductsByBrand = "SELECT * FROM " + getTablePrefix() + "_product WHERE brand_id = ? ORDER BY id ASC";
    private static final String queryGetProductsByOwner = "SELECT * FROM " + getTablePrefix() + "_product WHERE owner_id = ? ORDER BY id ASC";
    private static final String queryInsertProduct = "INSERT INTO " + getTablePrefix() + "_product (status, online, weight, price, name, description, inexpensive, professional, for_child, used, min_age, owner_id, insert_date, brand_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String queryRemoveProduct = "DELETE FROM " + getTablePrefix() + "_product WHERE id = ?";
    private static final String queryRemoveProductImages = "DELETE FROM " + getTablePrefix() + "_product_images WHERE product_id = ?";
    private static final String queryinsertProductImage = "INSERT INTO " + getTablePrefix() + "_product_images (image, product_id) VALUES (?,?)";
    private static final long serialVersionUID = 1L;

    public static void editProduct(ProductBean productBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryEditProduct)) {
            prepareStatement.setBoolean(1, productBean.isStatus());
            prepareStatement.setBoolean(2, productBean.isOnline());
            prepareStatement.setFloat(3, productBean.getWeight());
            prepareStatement.setFloat(4, productBean.getPrice());
            prepareStatement.setString(5, productBean.getName());
            prepareStatement.setString(6, productBean.getDescription());
            prepareStatement.setBoolean(7, productBean.isInexpensive());
            prepareStatement.setBoolean(8, productBean.isProfessional());
            prepareStatement.setBoolean(9, productBean.isForChild());
            prepareStatement.setBoolean(10, productBean.isUsed());
            prepareStatement.setInt(11, productBean.getMinAge());
            prepareStatement.setInt(12, productBean.getBrand().getID());
            prepareStatement.setInt(13, productBean.getID());
            prepareStatement.execute();
        }
        removeProductImages(productBean.getID());
        insertProductImages(productBean);
    }

    public static List<ProductBean> getOnlineProducts() throws SQLException {
        ArrayList<ProductBean> result = new ArrayList<>();
        try (Statement statement = getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(queryGetOnlineProducts)) {
                while (resultSet.next()) {
                    result.add(makeProduct(resultSet));
                }
            }
        }
        return result;
    }

    public static List<ProductBean> getProductsByOwner(int customerID) throws SQLException {
        ArrayList<ProductBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetProductsByOwner)) {
            preparedStatement.setInt(1, customerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeProduct(resultSet));
                }
            }
        }
        return result;
    }

    public static ProductBean getProduct(int productID) throws SQLException {
        ProductBean result = new ProductBean();
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryGetProduct)) {
            prepareStatement.setInt(1, productID);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = (makeProduct(resultSet));
                }
            }
        }
        return result;
    }

    public static List<String> getProductImages(int productID) throws SQLException {
        LinkedList<String> result = new LinkedList<>();
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryGetProductImages)) {
            prepareStatement.setInt(1, productID);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(resultSet.getString("image"));
                }
            }
        }
        return result;
    }

    public static List<ProductBean> getProducts(Map<String, Boolean> filters) throws SQLException {
        ArrayList<ProductBean> result = new ArrayList<>();
        String query = "SELECT * FROM " + getTablePrefix() + "_product ";
        if (filters.size() > 0) {
            query += "WHERE ";
            for (Iterator<Entry<String, Boolean>> it = filters.entrySet().iterator(); it.hasNext();) {
                Map.Entry<String, Boolean> entry = it.next();
                query = query.concat(entry.getKey().concat(" = ? "));
                if (it.hasNext()) {
                    query = query.concat("AND ");
                }
            }
            query += "ORDER BY id ASC";
        }
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(query)) {
            int i = 0;
            for (Entry<String, Boolean> entry : filters.entrySet()) {
                preparedStatement.setBoolean(++i, entry.getValue());
            }
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeProduct(resultSet));
                }
            }
        }
        return result;
    }

    public static List<ProductBean> getProductsByBrand(int brandID) throws SQLException {
        ArrayList<ProductBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetProductsByBrand)) {
            preparedStatement.setInt(1, brandID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeProduct(resultSet));
                }
            }
        }
        return result;
    }

    public static void insertProduct(ProductBean productBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryInsertProduct, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setBoolean(1, productBean.isStatus());
            prepareStatement.setBoolean(2, productBean.isOnline());
            prepareStatement.setFloat(3, productBean.getWeight());
            prepareStatement.setFloat(4, productBean.getPrice());
            prepareStatement.setString(5, productBean.getName());
            prepareStatement.setString(6, productBean.getDescription());
            prepareStatement.setBoolean(7, productBean.isInexpensive());
            prepareStatement.setBoolean(8, productBean.isProfessional());
            prepareStatement.setBoolean(9, productBean.isForChild());
            prepareStatement.setBoolean(10, productBean.isUsed());
            if (productBean.isForChild()) {
                prepareStatement.setInt(11, productBean.getMinAge());
            } else {
                prepareStatement.setNull(11, java.sql.Types.INTEGER);
            }
            if (productBean.isUsed() && productBean.getOwner().getID() > 0) {
                prepareStatement.setInt(12, productBean.getOwner().getID());
            } else {
                prepareStatement.setNull(12, java.sql.Types.INTEGER);
            }
            prepareStatement.setDate(13, java.sql.Date.valueOf(productBean.getInsertDate()));
            prepareStatement.setInt(14, productBean.getBrand().getID());
            int affectedRows = prepareStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating product failed, no rows affected.");
            }

            try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    productBean.setID(generatedKeys.getInt("id"));
                } else {
                    throw new SQLException("Creating product failed, no ID obtained.");
                }
            }
            insertProductImages(productBean);
        }
    }

    public static void removeProduct(int productID) throws SQLException {
        removeProductImages(productID);
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveProduct)) {
            prepareStatement.setInt(1, productID);
            prepareStatement.execute();
        }
    }

    private static void insertProductImages(ProductBean productBean) throws SQLException {
        for (String image : productBean.getProductImages()) {
            try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryinsertProductImage)) {
                prepareStatement.setString(1, image);
                prepareStatement.setInt(2, productBean.getID());
                prepareStatement.execute();
            }
        }
    }

    private static ProductBean makeProduct(ResultSet resultSet) throws SQLException {
        ProductBean productBean = new ProductBean();
        productBean.setID(resultSet.getInt("id"));
        productBean.setStatus(resultSet.getBoolean("status"));
        productBean.setOnline(resultSet.getBoolean("online"));
        productBean.setInexpensive(resultSet.getBoolean("inexpensive"));
        productBean.setWeight(resultSet.getFloat("weight"));
        productBean.setPrice(resultSet.getFloat("price"));
        productBean.setName(resultSet.getString("name"));
        productBean.setDescription(resultSet.getString("description"));
        productBean.setForChild(resultSet.getBoolean("for_child"));
        productBean.setMinAge(resultSet.getInt("min_age"));
        productBean.setProfessional(resultSet.getBoolean("professional"));
        productBean.setUsed(resultSet.getBoolean("used"));
        productBean.setInsertDate(resultSet.getDate("insert_date").toString());
        productBean.setOwner(CustomerModel.getProfessional(resultSet.getInt("owner_id")));
        productBean.setBrand(BrandModel.getBrand(resultSet.getInt("brand_id")));
        productBean.setProductImage(getProductImages(productBean.getID()));
        return productBean;
    }

    private static void removeProductImages(int productID) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveProductImages)) {
            prepareStatement.setInt(1, productID);
            prepareStatement.execute();
        }
    }

}
