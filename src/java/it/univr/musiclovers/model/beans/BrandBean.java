package it.univr.musiclovers.model.beans;

import java.io.Serializable;

public class BrandBean implements Serializable {

    private String description;
    private int ID;
    private String link;
    private String logo;
    private String name;
    private static final long serialVersionUID = 1L;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public String getLink() {
        if (link.startsWith("http://") || link.startsWith("https://")) {
            return link;
        } else {
            return "http://".concat(link);
        }
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
