package it.univr.musiclovers.model.beans;

import java.io.Serializable;

/**
 *
 * @author blasco991
 */
public class AccountBean implements Serializable {

    private int ID;
    private String password;
    private PersonBean person;
    private String username;
    private static final long serialVersionUID = 1L;

    public AccountBean(PersonBean personBean) {
        this.setPerson(personBean);
    }

    public AccountBean() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonBean getPerson() {
        return person;
    }

    public void setPerson(PersonBean person) {
        this.person = person;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEmployer() {
        return person instanceof EmployerBean;
    }

    public boolean isProfessional() {
        return person instanceof ProfessionalBean;
    }

}
