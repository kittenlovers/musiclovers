package it.univr.musiclovers.model.beans;

/**
 *
 * @author blasco991
 */
public class ProfessionalBean extends CustomerBean implements PersonBean {

    private int accountID;
    private int reduction;
    private String role;
    private static final long serialVersionUID = 1L;

    public ProfessionalBean() {
        super();
    }

    public ProfessionalBean(CustomerBean customerBean, String role, int reduction) {
        setID(customerBean.getID());
        setCity(customerBean.getCity());
        setCode(customerBean.getCode());
        setEmail(customerBean.getEmail());
        setMobile(customerBean.getMobile());
        setName(customerBean.getName());
        setSurname(customerBean.getSurname());
        setTelephone(customerBean.getTelephone());
        setRole(role);
        setReduction(reduction);
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getReduction() {
        return reduction;
    }

    public final void setReduction(int reduction) {
        this.reduction = reduction;
    }

    public String getRole() {
        return role;
    }

    public final void setRole(String role) {
        this.role = role;
    }

}
