package it.univr.musiclovers.model;

import static it.univr.musiclovers.model.Model.getConnection;
import static it.univr.musiclovers.model.Model.getTablePrefix;
import it.univr.musiclovers.model.beans.*;
import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marian Solomon
 */
public abstract class OrderModel extends Model {

    private static final String queryUpdateOrder = "UPDATE " + getTablePrefix() + "_order SET price = ?, sold_date = ?, payment_type = ?, product_id = ?, employer_id = ?, buyer_id = ? WHERE id = ?";
    private static final String queryGetEmployers = "SELECT * FROM " + getTablePrefix() + "_employer ORDER BY id ASC";
    private static final String queryGetOrder = "SELECT * FROM " + getTablePrefix() + "_order WHERE id = ?";
    private static final String queryGetOrders = "SELECT * FROM " + getTablePrefix() + "_order";
    private static final String queryGetOrdersByBuyer = "SELECT * FROM " + getTablePrefix() + "_order WHERE buyer_id = ?";
    private static final String queryGetOrdersByProduct = "SELECT * FROM " + getTablePrefix() + "_order WHERE product_id = ?";
    private static final String queryGetOrdersBySeller = "SELECT o.id AS id, o.price AS price, o.sold_date AS sold_date, o.payment_type AS payment_type, o.product_id AS product_id, o.employer_id AS employer_id, o.buyer_id AS buyer_id FROM " + getTablePrefix() + "_order AS o JOIN " + getTablePrefix() + "_product AS p ON o.product_id = p.id WHERE p.owner_id = ?";
    private static final String queryInsertOrder = "INSERT INTO " + getTablePrefix() + "_order (price, sold_date, payment_type, product_id, employer_id, buyer_id) VALUES (?,?,?,?,?,?)";
    private static final String queryRemoveOrder = "DELETE FROM " + getTablePrefix() + "_order WHERE id = ?";
    private static final long serialVersionUID = 1L;

    public static void editOrder(OrderBean orderBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryUpdateOrder)) {
            prepareStatement.setFloat(1, orderBean.getPrice());
            prepareStatement.setDate(2, Date.valueOf(orderBean.getSoldDate()));
            prepareStatement.setString(3, orderBean.getPaymentType());
            prepareStatement.setInt(4, orderBean.getProduct().getID());
            prepareStatement.setInt(5, orderBean.getSeller().getID());
            prepareStatement.setInt(6, orderBean.getBuyer().getID());
            prepareStatement.setInt(7, orderBean.getID());
            prepareStatement.execute();
        }
    }

    public static List<EmployerBean> getEmployers() throws SQLException {
        ArrayList<EmployerBean> result = new ArrayList<>();
        try (Statement statement = getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(queryGetEmployers)) {
                while (resultSet.next()) {
                    result.add(EmployerModel.getEmployerByAccount(resultSet.getInt("id")));
                }
            }
        }
        return result;
    }

    public static OrderBean getOrder(int orderID) throws SQLException, ParseException {
        OrderBean result = new OrderBean();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrder)) {
            preparedStatement.setInt(1, orderID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeOrderBean(resultSet);
                }
            }
        }
        return result;
    }

    public static List<OrderBean> getOrders() throws SQLException, ParseException {
        List<OrderBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrders)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeOrderBean(resultSet));
                }
            }
        }
        return result;
    }

    public static List<OrderBean> getOrdersByBuyer(int buyerID) throws SQLException, ParseException {
        List<OrderBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrdersByBuyer)) {
            preparedStatement.setInt(1, buyerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeOrderBean(resultSet));
                }
            }
        }
        return result;
    }

    public static List<OrderBean> getOrdersByCustomer(int customerID) throws SQLException, ParseException {
        List<OrderBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrdersByBuyer)) {
            preparedStatement.setInt(1, customerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeOrderBean(resultSet));
                }
            }
        }
        return result;
    }

    public static List<OrderBean> getOrdersByProduct(int productID) throws SQLException, ParseException {
        List<OrderBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrdersByProduct)) {
            preparedStatement.setInt(1, productID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeOrderBean(resultSet));
                }
            }
        }
        return result;
    }

    public static List<OrderBean> getOrdersBySeller(int sellerID) throws SQLException, ParseException {
        List<OrderBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetOrdersBySeller)) {
            preparedStatement.setInt(1, sellerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeOrderBean(resultSet));
                }
            }
        }
        return result;
    }

    public static void insertOrder(OrderBean orderBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryInsertOrder, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setFloat(1, orderBean.getPrice());
            prepareStatement.setDate(2, Date.valueOf(orderBean.getSoldDate()));
            prepareStatement.setString(3, orderBean.getPaymentType());
            prepareStatement.setInt(4, orderBean.getProduct().getID());
            prepareStatement.setInt(5, orderBean.getSeller().getID());
            prepareStatement.setInt(6, orderBean.getBuyer().getID());
            int affectedRows = prepareStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating order failed, no rows affected.");
            }

            try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    orderBean.setID(generatedKeys.getInt("id"));
                } else {
                    throw new SQLException("Creating order failed, no ID obtained.");
                }
            }
        }
    }

    public static void removeOrder(int orderID) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveOrder)) {
            prepareStatement.setInt(1, orderID);
            prepareStatement.execute();
        }
    }

    private static OrderBean makeOrderBean(ResultSet resultSet) throws SQLException, ParseException {
        OrderBean orderBean = new OrderBean();
        orderBean.setID(resultSet.getInt("id"));
        orderBean.setPrice(resultSet.getFloat("price"));
        orderBean.setSoldDate(resultSet.getDate("sold_date").toString());
        orderBean.setProduct(ProductModel.getProduct(resultSet.getInt("product_id")));
        orderBean.setSeller(EmployerModel.getEmployer(resultSet.getInt("employer_id")));
        orderBean.setBuyer(CustomerModel.getCustomer(resultSet.getInt("buyer_id")));
        orderBean.setPaymentType(resultSet.getString("payment_type"));
        return orderBean;
    }

}
