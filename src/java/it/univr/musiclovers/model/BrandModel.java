package it.univr.musiclovers.model;

import static it.univr.musiclovers.model.Model.getConnection;
import static it.univr.musiclovers.model.Model.getTablePrefix;
import it.univr.musiclovers.model.beans.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marian Solomon
 */
public abstract class BrandModel extends Model {

    private static final String queryEditBrand = "UPDATE " + getTablePrefix() + "_brand SET name = ?, description = ?, link = ?, logo = ? WHERE id = ?";
    private static final String queryGetBrand = "SELECT * FROM " + getTablePrefix() + "_brand WHERE id = ?";
    private static final String queryGetBrands = "SELECT * FROM " + getTablePrefix() + "_brand ORDER BY id ASC";
    private static final String queryInsertBrand = "INSERT INTO " + getTablePrefix() + "_brand (name, description, link, logo) VALUES (?,?,?,?)";
    private static final String queryRemoveBrand = "DELETE FROM " + getTablePrefix() + "_BRAND WHERE id = ?";
    private static final String queryGetBrandsByProduct = "SELECT * FROM " + getTablePrefix() + "_product WHERE brand_id = ?";

    private static final long serialVersionUID = 1L;

    public static void editBrand(BrandBean selectedBrand) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryEditBrand)) {
            prepareStatement.setString(1, selectedBrand.getName());
            prepareStatement.setString(2, selectedBrand.getDescription());
            prepareStatement.setString(3, selectedBrand.getLink());
            prepareStatement.setString(4, selectedBrand.getLogo());
            prepareStatement.setInt(5, selectedBrand.getID());
            prepareStatement.execute();
        }
    }

    public static BrandBean getBrand(int id) throws SQLException {
        BrandBean result = new BrandBean();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetBrand)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeBrandBean(resultSet);
                }
            }
        }
        return result;
    }

    public static List<BrandBean> getBrands() throws SQLException {
        List<BrandBean> result = new ArrayList<>();
        try (Statement statement = getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(queryGetBrands)) {
                while (resultSet.next()) {
                    result.add(makeBrandBean(resultSet));
                }
            }
        }
        return result;
    }

    public static List<BrandBean> getBrandsByProduct(int brandID) throws SQLException, ParseException {
        List<BrandBean> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetBrandsByProduct)) {
            preparedStatement.setInt(1, brandID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(makeBrandBean(resultSet));
                }
            }
        }
        return result;
    }

    public static void insertBrand(BrandBean selectedBrand) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryInsertBrand,
                Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, selectedBrand.getName());
            prepareStatement.setString(2, selectedBrand.getDescription());
            prepareStatement.setString(3, selectedBrand.getLink());
            prepareStatement.setString(4, selectedBrand.getLogo());
            prepareStatement.executeUpdate();
            
            try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    selectedBrand.setID(generatedKeys.getInt("id"));
                } else {
                    throw new SQLException("Creating brand failed, no ID obtained.");
                }
            }
        }
    }

    public static void removeBrand(int brandID) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveBrand)) {
            prepareStatement.setInt(1, brandID);
            prepareStatement.execute();
        }
    }

    private static BrandBean makeBrandBean(ResultSet resultSet) throws SQLException {
        BrandBean brandBean = new BrandBean();
        brandBean.setID(resultSet.getInt("id"));
        brandBean.setName(resultSet.getString("name"));
        brandBean.setDescription(resultSet.getString("description"));
        brandBean.setLink(resultSet.getString(4));
        brandBean.setLogo(resultSet.getString(5));
        return brandBean;
    }

}
