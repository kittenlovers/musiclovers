package it.univr.musiclovers.model;

import java.io.Serializable;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Marian Solomon
 */
public abstract class Model implements Serializable {

    private static final long serialVersionUID = 1L;

    public static Connection getConnection() {
        return ConnectionModel.getConnection();
    }

    public static String getTablePrefix() {
        return ConnectionModel.getTablePrefix();
    }

    public static String today() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

}
