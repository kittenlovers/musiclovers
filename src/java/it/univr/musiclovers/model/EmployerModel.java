package it.univr.musiclovers.model;

import static it.univr.musiclovers.model.CustomerModel.getProfessional;
import static it.univr.musiclovers.model.Model.getConnection;
import it.univr.musiclovers.model.beans.AccountBean;
import it.univr.musiclovers.model.beans.EmployerBean;
import it.univr.musiclovers.model.beans.ProfessionalBean;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marian Solomon
 */
public abstract class EmployerModel extends Model {

    private static final String queryUpdateEmployer = "UPDATE " + getTablePrefix() + "_employer SET code = ?, birthdate = ?, name = ?, surname = ?  WHERE id = ?";
    private static final String queryGetAccount = "SELECT a.id as id, a.username, a.password FROM " + getTablePrefix() + "_account AS a JOIN " + getTablePrefix() + "_employer AS e ON a.id = e.account_id WHERE e.id = ?";
    private static final String queryGetAccountByUsrAndPwd = "SELECT * FROM " + getTablePrefix() + "_account WHERE username = ? AND password = ?";
    private static final String queryGetEmployer = "SELECT * FROM " + getTablePrefix() + "_employer WHERE id = ?";
    private static final String queryGetEmployerByAccount = "SELECT * FROM " + getTablePrefix() + "_employer WHERE account_id = ?";
    private static final String queryGetEmployers = "SELECT * FROM " + getTablePrefix() + "_employer";
    private static final String queryInsertAccount = "INSERT INTO " + getTablePrefix() + "_account (username, password) VALUES (?,?)";
    private static final String queryInsertEmployer = "INSERT INTO " + getTablePrefix() + "_employer (code, name, surname, birthdate, account_id) VALUES (?,?,?,?,?)";
    private static final String queryUpdateAccount = "UPDATE " + getTablePrefix() + "_account SET username = ?, password = ? WHERE id = ?";
    private static final String queryRemoveAccount = "DELETE FROM " + getTablePrefix() + "_account WHERE id = ?";
    private static final String queryRemoveEmployer = "DELETE FROM " + getTablePrefix() + "_employer WHERE id = ?";
    private static final long serialVersionUID = 1L;

    public static void editEmployer(AccountBean accountBean) throws SQLException {
        EmployerBean employerBean = (EmployerBean) accountBean.getPerson();
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryUpdateEmployer)) {
            prepareStatement.setString(1, employerBean.getCode());
            prepareStatement.setDate(2, java.sql.Date.valueOf(employerBean.getBirthDate()));
            prepareStatement.setString(3, employerBean.getName());
            prepareStatement.setString(4, employerBean.getSurname());
            prepareStatement.setInt(5, employerBean.getID());
            prepareStatement.execute();
        }
        editAccount(accountBean);
    }

    public static AccountBean getAccount(String username, String password) throws SQLException {
        AccountBean result = null;
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetAccountByUsrAndPwd)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeEmployerAccountBean(resultSet);
                }
            }
        }
        return result;
    }

    public static AccountBean getAccountByEmployer(int employerID) throws SQLException {
        AccountBean result = new AccountBean(new EmployerBean());
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetAccount)) {
            preparedStatement.setInt(1, employerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeEmployerAccountBean(resultSet);
                }
            }
        }
        return result;
    }

    public static EmployerBean getEmployer(int employerID) throws SQLException {
        EmployerBean result = null;
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetEmployer)) {
            preparedStatement.setInt(1, employerID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeEmployerBean(resultSet);
                }
            }
        }
        return result;
    }

    public static EmployerBean getEmployerByAccount(int accountID) throws SQLException {
        EmployerBean result = null;
        try (PreparedStatement preparedStatement = getConnection().prepareStatement(queryGetEmployerByAccount)) {
            preparedStatement.setInt(1, accountID);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    result = makeEmployerBean(resultSet);
                }
            }
        }
        return result;
    }

    public static List<EmployerBean> getEmployers() throws SQLException {
        List<EmployerBean> result = new ArrayList<>();
        try (Statement statement = getConnection().createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(queryGetEmployers)) {
                while (resultSet.next()) {
                    result.add(makeEmployerBean(resultSet));
                }
            }
        }
        return result;
    }

    public static void insertEmployer(AccountBean accountBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryInsertAccount, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, String.valueOf(Math.random()));
            prepareStatement.setString(2, accountBean.getPassword());
            int affectedRows = prepareStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating account failed, no rows affected.");
            }

            try (ResultSet generatedKeys = prepareStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    accountBean.setID(generatedKeys.getInt("id"));
                } else {
                    throw new SQLException("Creating account failed, no ID obtained.");
                }
            }
        }
        EmployerBean employerBean = ((EmployerBean) accountBean.getPerson());
        employerBean.setCode(employerBean.getName().concat(String.valueOf(accountBean.getID())));
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryInsertEmployer, Statement.RETURN_GENERATED_KEYS)) {
            prepareStatement.setString(1, employerBean.getCode());
            prepareStatement.setString(2, employerBean.getName());
            prepareStatement.setString(3, employerBean.getSurname());
            prepareStatement.setDate(4, Date.valueOf(employerBean.getBirthDate()));
            prepareStatement.setInt(5, accountBean.getID());
            prepareStatement.executeUpdate();
        }
        accountBean.setUsername(employerBean.getCode() + "@kittenlovers.com");
        editAccount(accountBean);
    }

    public static void removeEmployer(int employerID) throws SQLException {
        AccountBean accountBean = getAccountByEmployer(employerID);
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveEmployer)) {
            prepareStatement.setInt(1, employerID);
            prepareStatement.execute();
        }
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryRemoveAccount)) {
            prepareStatement.setInt(1, accountBean.getID());
            prepareStatement.execute();
        }
    }

    private static void editAccount(AccountBean accountBean) throws SQLException {
        try (PreparedStatement prepareStatement = getConnection().prepareStatement(queryUpdateAccount)) {
            prepareStatement.setString(1, accountBean.getUsername());
            prepareStatement.setString(2, accountBean.getPassword());
            prepareStatement.setInt(3, accountBean.getID());
            prepareStatement.execute();
        }
    }

    private static AccountBean makeEmployerAccountBean(ResultSet resultSet) throws SQLException {
        AccountBean accountBean = new AccountBean();
        accountBean.setID(resultSet.getInt("id"));
        accountBean.setUsername(resultSet.getString("username"));
        accountBean.setPerson(getEmployerByAccount(accountBean.getID()));
        return accountBean;
    }

    private static EmployerBean makeEmployerBean(ResultSet resultSet) throws SQLException {
        EmployerBean employerBean = new EmployerBean();
        employerBean.setID(resultSet.getInt("id"));
        employerBean.setName(resultSet.getString("name"));
        employerBean.setSurname(resultSet.getString("surname"));
        employerBean.setCode(resultSet.getString("code"));
        employerBean.setBirthDate(resultSet.getDate("birthdate").toString());
        return employerBean;
    }
}
